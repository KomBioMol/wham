# WHAM 

A fast and flexible WHAM implementation written in Python for ease of use and compatibility with Alan Grossfield's version, with several extended functionalities (anharmonic potentials and external weights, multiple inputs at a time, PMF plotting).

## Getting Started

The script is ready to use on any machine employed with a working Python distribution. Run `python wham.py -h` to see available options.



### Prerequisites

Any scientific Python distribution (such as Anaconda or WinPython) will typically contain all required libraries. Numpy, pandas, scipy and matplotlib are required for full functionality; if not available, they can be installed using pip:

```
pip install numpy scipy matplotlib pandas
```

### Options

#### Simple usage

  In its simplest form, the script can be used similarly to the popular Alan Grossfield's implementation (and is similarly fast), i.e., using a metafile in the format

```
/path/to/data/file/1   potential_center1   force_constant1
/path/to/data/file/2   potential_center2   force_constant2
...
```

  where the data file consists of two whitespace-separated columns (time:CV; the actual time column is not used). The default unit for energy-related quantities is kcal/mol, and the force constants match the formula V = ½k(x-x0)^2, i.e. with the pre-factor of 1/2 (this differs in different MD codes).

#### User-defined weights

  If the timeframes need to be weighted (e.g. due to the action of an orthogonal biasing potential), a third column can be added to the data file (so that it is formatted as time:CV:weight). This will be detected automatically, and no further action is needed.

#### Non-harmonic potentials

  If the potentials are not harmonic, one can supply external potentials in a tabulated form, as a two-column file (CV:value of the biasing potential), and then list them in the second column of the metafile:

```
/path/to/data/file/1   /path/to/external/potential/1
/path/to/data/file/2   /path/to/external/potential/2
...
```

  The use of non-harmonic potentials will be recognized based on the presence of two columns in the metafile, so there is no need to do anything else.

#### Error analysis

  A standard bootstrap protocol is available to provide error estimates for the free energy profiles.
  To perform error analysis, simply add another (rightmost) column to the metafile in which autocorrelation
  times for each window will be listed; this will be detected automatically. The standard number
  of bootstrap iterations is 50, but can be changed with the `-b` option. A sample metafile can look like this:

  ```
  /path/to/data/file/1   potential_center1   force_constant1   acorr_time_1
  /path/to/data/file/2   potential_center2   force_constant2   acorr_time_2
  ...
  ```

  or this:

  ```
  /path/to/data/file/1   /path/to/external/potential/1   acorr_time_1
  /path/to/data/file/2   /path/to/external/potential/2   acorr_time_2
  ...
  ```

#### Plotting the resulting free energy profile

  If desired, the free energy can be plotted simply by adding the `-p` flag in the command line.

#### Running WHAM many times at once

  By passing many metafiles at once using quotation marks (`-m "metafile1 metafile2 metafile3 ..."`),
  WHAM will be executed separately for each dataset. If the -p flag is included, all resulting
  free energy profiles will be shown next to each other.

  Each metafile is processed independently, so that different options (error analysis, harmonic/non-harmonic
  potentials etc.) can be mixed within one run.

### Examples

  To run WHAM on a single metafile with default parameters and plot the resulting profile:

```
python wham.py -m meta -p
```

  Multiple WHAM runs, plotted profiles saved to file profiles.svg:

```
python wham.py -m "meta1 meta2 meta3 meta4" -g profiles.svg
```

  General usage (parameters in square brackets are optional):

```
python wham.py -m metafile [-o FEP_outfile -f F_outfile -e stderr_outfile -T temperature -t tolerance -n num_bins -b bootstrap_iters -g graph_outfile -p]
```
